package main

import (
	"fmt"
	"log"
	"time"

	"github.com/alitto/pond"
	"github.com/beanstalkd/go-beanstalk"
)

func main() {
	log.Println("Worker running")

	conn, err := beanstalk.Dial("tcp", "localhost:11300")
	if err != nil {
		log.Fatal(err)
	}

	pool := pond.New(10, 0, pond.MinWorkers(10))
	defer pool.StopAndWait()

	err = worker(pool, conn, func(id uint64, body []byte) error {
		// implement business logic here
		fmt.Println(id, string(body))

		// ...then delete the job once we're finished.
		if err := conn.Delete(id); err != nil {
			return err
		}

		return nil
	})

	log.Fatal(err)
}

func worker(pool *pond.WorkerPool, conn *beanstalk.Conn, fn func(id uint64, body []byte) error) error {
	for {
		id, body, err := conn.Reserve(5 * time.Second)
		if err != nil {
			// If we're unable to reserve jobs, something's gone wrong. Quit
			// the consumer.
			return err
		}

		pool.Submit(func() {
			if err := fn(id, body); err != nil {
				log.Printf("unable to process job %d: %+v", id, err)

				err = conn.Bury(id, 0)
				if err != nil {
					// If we're having issues burying errored jobs, something's gone
					// wrong. quit the consumer
					log.Fatal(err)
				}
			}
		})
	}
}
