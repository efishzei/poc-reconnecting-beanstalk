module github.com/zeihanaulia/poc-reconnecting-beanstalk/worker

go 1.17

require (
	github.com/alitto/pond v1.7.0 // indirect
	github.com/beanstalkd/go-beanstalk v0.1.0 // indirect
)
