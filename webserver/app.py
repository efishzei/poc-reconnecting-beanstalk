from flask import Flask, jsonify
import logging
import greenstalk
import errno
from retry import retry

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)

app = Flask(__name__)

queue = greenstalk.Client(('beanstalkd', 11300))

need_reconnect = False

class NetworkFailureException(Exception):
    pass

@app.route("/")
def hello():
    return app.send_static_file("index.html")

@app.route("/publish")
def publish():

    try:
        job_id = publish_to_queue()
    except Exception as e:
        return jsonify({"error": "cannot publish to queue"}), 500

    return jsonify({'status':'ok','job_id': job_id})

@app.route("/read")
def read():
    
    try:
        job = read_queue()
    except Exception as e:
        return jsonify({"error": "cannot read from queue"}), 500

    return jsonify({'message':job.body})

@retry(NetworkFailureException, delay=5, tries=6)
def publish_to_queue():
    global need_reconnect
    
    try:
        if need_reconnect:
            reconnect()

        job_id = queue.put(b'hello')
        logging.info(job_id)

        need_reconnect = False
    except ConnectionError as e: # Catch connection error
        need_reconnect = True
        logging.error(e.errno)
        logging.error(e.__doc__)
        logging.error(e)
        raise NetworkFailureException()
    except IOError as e:
        need_reconnect = True
        logging.error(e.errno)
        logging.error(e.__doc__)
        logging.error(e)
        raise NetworkFailureException()
    except Exception as e:
        logging.error(e.__doc__)
        logging.error(e)
        raise
     
    return job_id

@retry(NetworkFailureException, delay=5, tries=6)
def read_queue():
    global need_reconnect

    try:
        if need_reconnect:
            reconnect()

        job = queue.reserve()
        print(job.id, job.body)
        queue.delete(job)
        queue.close()

        logging.info(job.id)
        need_reconnect = False
    except ConnectionError as e: # Catch connection error
        need_reconnect = True
        logging.error(e.errno)
        logging.error(e.__doc__)
        logging.error(e)
        raise NetworkFailureException()
    except IOError as e:
        need_reconnect = True
        logging.error(e.errno)
        logging.error(e.__doc__)
        logging.error(e)
        raise NetworkFailureException()
    except Exception as e:
        logging.error(e.__doc__)
        logging.error(e)
        raise
    
    return job

def reconnect():
    global queue
    logging.info("reconnecting to beanstalk")
    queue = greenstalk.Client(('beanstalkd', 11300))


if __name__ == '__main__':
    app.run(host='0.0.0.0')
