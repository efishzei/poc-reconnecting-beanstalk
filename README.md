# POC Auto-Reconnect Beanstalkd

Isssue saat ini beanstalk kadang mati dan harus direstart untuk membuat koneksi kembali.

Ada dua pendekatan untuk saat ini.

1. Menggunakan retry
2. Buka tutup koneksi per request.

## Approach

### Using Retry Mechanism

```bash
# 1. Running 
docker-compose up -d

# 2. Test publish
curl http://localhost:9000/publish

or

while true; do sleep 1; curl http://localhost:9000/publish; echo -e '\n\n\n\n'$(date);done

# 3. Test read
curl http://localhost:9000/read

# 4. Matikan beanstalk
docker-compose stop beanstalkd

# 5. Hit publish lagi
curl http://localhost:9000/publish

# 6. Webserver akan melakukan retry dengan jeda konstan selama 5 detik dan 6 kali pengulangan
# Variable bisa diubah ubah

web_1         | ERROR:root:None
web_1         | ERROR:root:Connection error.
web_1         | ERROR:root:Unexpected EOF
web_1         | WARNING:retry.api:, retrying in 5 seconds...
web_1         | INFO:root:reconnecting to beanstalk
web_1         | ERROR:root:-3
web_1         | ERROR:root:None
web_1         | ERROR:root:[Errno -3] Temporary failure in name resolution
web_1         | WARNING:retry.api:, retrying in 5 seconds...
web_1         | INFO:root:reconnecting to beanstalk
web_1         | ERROR:root:-3
web_1         | ERROR:root:None
web_1         | ERROR:root:[Errno -3] Temporary failure in name resolution
web_1         | WARNING:retry.api:, retrying in 5 seconds...
web_1         | INFO:root:reconnecting to beanstalk
web_1         | ERROR:root:-3
web_1         | ERROR:root:None
web_1         | ERROR:root:[Errno -3] Temporary failure in name resolution
web_1         | WARNING:retry.api:, retrying in 5 seconds...
web_1         | INFO:root:reconnecting to beanstalk
web_1         | ERROR:root:-3
web_1         | ERROR:root:None
web_1         | ERROR:root:[Errno -3] Temporary failure in name resolution
web_1         | WARNING:retry.api:, retrying in 5 seconds...
web_1         | INFO:root:reconnecting to beanstalk
web_1         | ERROR:root:-3
web_1         | ERROR:root:None
web_1         | ERROR:root:[Errno -3] Temporary failure in name resolution
web_1         | INFO:werkzeug:172.26.0.1 - - [17/Jan/2022 23:03:39] "GET /publish HTTP/1.1" 500 -
web_1         | INFO:root:reconnecting to beanstalk
web_1         | ERROR:root:-3
web_1         | ERROR:root:None
web_1         | ERROR:root:[Errno -3] Temporary failure in name resolution
web_1         | WARNING:retry.api:, retrying in 5 seconds...
web_1         | INFO:root:reconnecting to beanstalk
web_1         | INFO:root:1
web_1         | INFO:werkzeug:172.26.0.1 - - [17/Jan/2022 23:04:08] "GET /publish HTTP/1.1" 200 -

# 7. Nyalakan kembali beanstalk

docker-compose start beanstalkd

# 8. Hit publish lagi seharusnya langsung connect
curl http://localhost:9000/publish

```

#### Future update

- Tambahkan circuit breaker, untuk mencegah [thundering herd problem](https://en.wikipedia.org/wiki/Thundering_herd_problem)
- Perlukah Webserver dibuat async, karena kalau sync ketika proses retry client akan menunggu.

### Create and Close Per Request

Sepertinya ini kurang oke solusinya. Kalau requestnya banyak misal 1000 pada saat yang bersamaan. 
Mungkin akan kena connection limit.

- https://groups.google.com/g/beanstalk-talk/c/ocFCDGoVelg
- https://stackoverflow.com/questions/44253490/beanstalkd-too-many-open-files-error


## Decisision

Lebih baik menggunakan Approach pertama. Membuat retry secara berkala. kalau bisa backoff exponential.